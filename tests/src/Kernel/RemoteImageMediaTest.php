<?php

namespace Drupal\Tests\media_remote_image\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\media\Entity\Media;
use Drupal\media_remote_image_test\ResourceMiddleware;

/**
 * Tests creating media items from remote image sources.
 *
 * @group media_remote_image
 */
class RemoteImageMediaTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'file',
    'image',
    'media',
    'media_remote_image',
    'media_remote_image_test',
    'system',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Set up the database so that we can create remote image media items.
    $this->installConfig('media');
    $this->installConfig('media_remote_image');
    $this->installEntitySchema('file');
    $this->installEntitySchema('media');
    $this->installEntitySchema('user');
    $this->installSchema('file', ['file_usage']);
  }

  /**
   * Data provider for ::testRemoteImageMedia().
   *
   * @return array[]
   *   The test cases.
   */
  public static function providerRemoteImageMedia(): array {
    return [
      'Flickr' => [
        'https://www.flickr.com/photos/ukbuses/53942998495/in/photolist-2qbKY7c-2qcqxNR',
        __DIR__ . '/../../fixtures/flickr.json',
      ],
      'Getty Images' => [
        'http://www.gettyimages.com/detail/1071104734',
        __DIR__ . '/../../fixtures/gettyimages.json',
      ],
      'GIPHY' => [
        'https://giphy.com/gifs/news-march-on-washington-1963-IArfvsrvQt5JF4KPhR',
        __DIR__ . '/../../fixtures/giphy.json',
      ],
    ];
  }

  /**
   * Tests creating remote image media from various providers.
   *
   * @param string $url
   *   The URL of the remote media to add. In a real site, this would be used to
   *   create the media in the UI.
   * @param string $fixture_file
   *   The path of the local file containing the oEmbed resource data.
   *
   * @dataProvider providerRemoteImageMedia
   */
  public function testRemoteImageMedia(string $url, string $fixture_file): void {
    $media = $this->createRemoteImageMedia($url, $fixture_file);

    // The resource title should be reused as the alt text of the thumbnail.
    $source = $media->getSource();
    $plugin_definition = $source->getPluginDefinition();
    $this->assertSame($media->label(), $source->getMetadata($media, $plugin_definition['thumbnail_alt_metadata_attribute']));
  }

  /**
   * Creates a remote image media item.
   *
   * @param string $url
   *   The URL of the remote media to add. In a real site, this would be used to
   *   create the media in the UI.
   * @param string $fixture_file
   *   The path of the local file containing the oEmbed resource data.
   *
   * @return \Drupal\media\Entity\Media
   *   The created media item.
   */
  private function createRemoteImageMedia(string $url, string $fixture_file): Media {
    // Ensure the resource data can be fetched by the test middleware.
    $resource_url = $this->container->get('media.oembed.url_resolver')
      ->getResourceUrl($url);

    /** @var \Drupal\Core\State\StateInterface $state */
    $state = $this->container->get('state');
    $paths = [
      $resource_url => $fixture_file,
    ];
    $state->set(ResourceMiddleware::class, $paths);

    // Ensure the the thumbnail can be fetched by our test middleware.
    $thumbnail_url = $this->container->get('media.oembed.resource_fetcher')
      ->fetchResource($resource_url)
      ->getThumbnailUrl();
    $this->assertNotEmpty($thumbnail_url);
    $thumbnail_url = $thumbnail_url->toString();
    $paths[$thumbnail_url] = $this->getDrupalRoot() . '/core/misc/druplicon.png';
    $state->set(ResourceMiddleware::class, $paths);

    // Ensure we can create a remote image media item from the given fixture.
    $media = Media::create([
      'bundle' => 'remote_image',
      'field_media_oembed_image' => $url,
    ]);
    $media->save();

    return $media;
  }

}

