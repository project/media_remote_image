<?php

namespace Drupal\media_remote_image_test;

use Drupal\Core\State\StateInterface;
use GuzzleHttp\Promise\Create;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\RequestInterface;
use Symfony\Component\Mime\MimeTypeGuesserInterface;

/**
 * Serves fixture files to Guzzle when specific URLs are requested.
 */
final class ResourceMiddleware {

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  private $state;

  /**
   * The MIME type guesser service.
   *
   * @var \Symfony\Component\Mime\MimeTypeGuesserInterface
   */
  private $mimeTypeGuesser;

  /**
   * Constructs a ResourceMiddleware object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Symfony\Component\Mime\MimeTypeGuesserInterface $mime_type_guesser
   *   The MIME type guesser service.
   */
  public function __construct(StateInterface $state, MimeTypeGuesserInterface $mime_type_guesser) {
    $this->state = $state;
    $this->mimeTypeGuesser = $mime_type_guesser;
  }

  /**
   * Serves file contents to Guzzle when particular URLs are requested.
   */
  public function __invoke() {
    return function ($handler) {
      return function (RequestInterface $request, array $options) use ($handler): PromiseInterface {
        $uri = (string) $request->getUri();
        $paths = $this->state->get(static::class, []);
        if (array_key_exists($uri, $paths)) {
          $path = $paths[$uri];
          $response = new Response(200, [], file_get_contents($path));

          $mime_type = $this->mimeTypeGuesser->guessMimeType($path);
          if ($mime_type) {
            $response = $response->withHeader('Content-Type', $mime_type);
          }
          return Create::promiseFor($response);
        }
        return $handler($request, $options);
      };
    };
  }

}
