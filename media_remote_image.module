<?php

/**
 * @file
 * Implements hook_media_source_info_alter() for the following providers: Flickr, Getty Images, GIPHY
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\media\Plugin\media\Source\OEmbed;

/**
 * Implements hook_help().
 */
function media_remote_image_help($route_name, RouteMatchInterface $arg) {
  switch ($route_name) {
    case 'help.page.media_remote_image':
      $output = '';
      $output .= '<p>' . t('Creates a Remote image media type for easily adding oEmbed images') . '</p>';
      return ['#markup' => $output];
  }
}

/**
 * Implements hook_media_source_info_alter().
 */
function media_remote_image_media_source_info_alter(array &$definitions) {
  // Reuse certain values from core's oEmbed video source.
  $base_definition = $definitions['oembed:video'];

  $definitions['oembed:image'] = [
    'id' => 'oembed:image',
    'label' => t('Remote image'),
    'description' => t('Add images from Flickr, Getty Images, GIPHY.'),
    'allowed_field_types' => ['string'],
    'default_name_metadata_attribute' => $base_definition['default_name_metadata_attribute'],
    'default_thumbnail_filename' => 'no-thumbnail.png',
    'thumbnail_uri_metadata_attribute' => $base_definition['thumbnail_uri_metadata_attribute'],
    'thumbnail_width_metadata_attribute' => $base_definition['thumbnail_width_metadata_attribute'],
    'thumbnail_height_metadata_attribute' => $base_definition['thumbnail_height_metadata_attribute'],
    'thumbnail_alt_metadata_attribute' => 'title',
    'providers' => ['Flickr', 'Getty Images', 'GIPHY'],
    'class' => OEmbed::class,
    'provider' => 'media_remote_image',
  ];
  if (Drupal::moduleHandler()->moduleExists('media_library')) {
    $definitions['oembed:image']['forms']['media_library_add'] = $base_definition['forms']['media_library_add'];
  }
}
