<?php

/**
 * @file
 * Contains post-update hooks for Media Remote Image.
 */

/**
 * Ensures the remote image media source has the correct metadata attributes.
 */
function media_remote_image_post_update_fix_thumbnail_metadata_attributes() {
  // Deliberately empty in order to force a container rebuild.
}
