# Media Remote Image

A simple module that extends oEmbed support added to Drupal Core's Media module
by implementing hook_media_source_info_alter() for oEmbed image providers.


## Requirements

Drupal Core Media (media) module.


## Installation

`composer require drupal/media_remote_image:^1.0`


## Configuration

This module does not have configuration.


## Features

- **Media type:** Creates a 'Remote image' Media type
- **Manage fields:** Creates an 'Image URL' (field_media_oembed_image) plain
  text field
- **Manage form display:** Uses the 'oEmbed URL' widget
- **Manage display:** Uses the 'oEmbed content' widget


## Usage:

To add Remote image content, simply go to `/media/add/remote_image`
and paste the image url.
